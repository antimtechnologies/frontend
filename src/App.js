import React, { Component } from 'react';

import { QueryRenderer, graphql } from 'react-relay';
import environment from './Environment';

const AppAllPostQuery = graphql`
  query AppAllPostQuery {
    viewers {
      id
    }
  }
`;
class App extends Component {
  render() {
    return (
      <QueryRenderer
        environment={environment}
        query={AppAllPostQuery}
        render={({ error, props }) => {
          if (error) {
            return <div>{error.message}</div>;
          }
          else if (props) {
            console.log('props', props);
            return <div>demo</div>
          }
          return <div>Loading</div>;
        }}
      />
    );
  }
}

export default App;
