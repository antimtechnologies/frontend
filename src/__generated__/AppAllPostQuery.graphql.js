/**
 * @flow
 * @relayHash fa22b15e14931c6db93cfe174b4047b2
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type AppAllPostQueryVariables = {||};
export type AppAllPostQueryResponse = {|
  +viewers: {|
    +id: string
  |}
|};
export type AppAllPostQuery = {|
  variables: AppAllPostQueryVariables,
  response: AppAllPostQueryResponse,
|};
*/


/*
query AppAllPostQuery {
  viewers {
    id
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "viewers",
    "storageKey": null,
    "args": null,
    "concreteType": "Viewer",
    "plural": false,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "id",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "AppAllPostQuery",
  "id": null,
  "text": "query AppAllPostQuery {\n  viewers {\n    id\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "AppAllPostQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": v0
  },
  "operation": {
    "kind": "Operation",
    "name": "AppAllPostQuery",
    "argumentDefinitions": [],
    "selections": v0
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'eb032f714548a3a0772be914dc900577';
module.exports = node;
